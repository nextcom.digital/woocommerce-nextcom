<?php
/**
 * WooCommerce Nextcom Digital
 *
 * PHP version 7
 *
 * @category  Wordpress_Plugin
 * @package   Nxt
 * @author    Nextcom Digital <contato@nextcom.digital>
 * @copyright 2019 Nextcom Digital
 * @license   Proprietary https://nextcom.digital
 * @link      https://nextcom.digital
 *
 * @wordpress-plugin
 * Plugin Name: WooCommerce Nextcom Digital
 * Plugin URI:  https://nextcom.digital
 * Description: WooCommerce Nextcom Digital custom plugin
 * Version:     1.0.0
 * Author:      Nextcom Digital
 * Author URI:  https://nextcom.digital/
 * Text Domain: wc_nextcom
 * Copyright: © 2019 Nextcom Digital (email : developer@acme.com)
 * License:     Proprietary
 * License URI: https://nextcom.digital
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Check if WooCommerce is active
 */
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

    if (! class_exists('WC_Nextcom')) {
        
        /**
         * Localisation
         **/
        load_plugin_textdomain('wc_nextcom', false, dirname(plugin_basename(__FILE__)) . '/languages');

        class WC_Nextcom 
        {
            public function __construct() 
            {
                // called only after woocommerce has finished loading
                add_action('woocommerce_init', array(&$this, 'woocommerce_loaded'));
                
                // called after all plugins have loaded
                add_action('plugins_loaded', array(&$this, 'plugins_loaded'));
                
                // called just before the woocommerce template functions are included
                add_action('init', array(&$this, 'include_template_functions'), 20);
                
                // indicates we are running the admin
                if (is_admin()) {
                    // ...
                }
                
                // indicates we are being served over ssl
                if (is_ssl()) {
                    // ...
                }
    
                // take care of anything else that needs to be done immediately upon plugin instantiation, here in the constructor
            }
            
            /**
             * Take care of anything that needs woocommerce to be loaded.  
             * For instance, if you need access to the $woocommerce global
             * 
             * @return void
             */
            public function woocommerce_loaded() 
            {
                // ...
            }
            
            /**
             * Take care of anything that needs all plugins to be loaded
             * 
             * @return void
             */
            public function plugins_loaded() 
            {
                // ...
            }
            
            /**
             * Override any of the template functions from woocommerce/woocommerce-template.php 
             * with our own template functions file
             * 
             * @return void
             */
            public function include_template_functions() 
            {
                include 'woocommerce-template.php';
            }
        }

        // finally instantiate our plugin class and add it to the set of globals
        $GLOBALS['wc_nextcom'] = new WC_Nextcom();
    }
}